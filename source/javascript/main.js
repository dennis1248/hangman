var letter;
var word;
var correct = 0;
var attempts = 0;
var restartClicked = 0;
var guessOk = false;
var posted = false;
var post = "";

var letterInput = document.getElementById("letter_input");
var headerTarget = document.getElementById("target");
var imageTarget = document.getElementById("image");
var restartWarning = document.getElementById("restart_warning");
var wordInput = document.getElementById("word_input");

var buttonSend = document.getElementById("button_send");
var buttonSendWord = document.getElementById("button_send_word");

// Replace char in string
function setCharAt(str,index,chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}

// Load value from letter input and check if it has any matches
async function check() {
  if (word === undefined || word == "") {
    headerTarget.innerHTML = "# No word defined yet #";
  } else {
    letter = letterInput.value;
    letterInput.value = "";

    for (var i = 0; i < word.length; i++) {
      if (word[i] === letter) {
        post = setCharAt(post, i, letter);
        headerTarget.innerHTML = post;
        correct++;
        guessOk = true;
      }
    }

    // If guess is WRONG
    if (guessOk !== true) {
      attempts++;
      imageTarget.setAttribute("src", "/images/" + attempts + ".png");
    }

    // Lose scenario
    if (attempts >= 11) {
      headerTarget.innerHTML = word;
      buttonSend.disabled = true;
      letterInput.disabled = true;
      await new Promise(r => setTimeout(r, 2000));
      imageTarget.setAttribute("src", "/images/loser.gif");
    }

    // Win scenario
    if (correct == word.length) {
      imageTarget.setAttribute("src", "/images/winner.gif");
      buttonSend.disabled = true;
      letterInput.disabled = true;
    }
    guessOk = false;
    letterInput.select();
  }
}

// Set the word and print secret text to headerTarget
function setWord() {
  document.getElementById("image").setAttribute("src", "/images/0.png"); // STOP THE FUCKING ANIMATION
  word = wordInput.value.toLowerCase().replace(/ /g,'');
  wordInput.value = "";
  if (word === "" || word === undefined) {
    headerTarget.innerHTML = "# Word input box empty #";
    return;
  }
  for (var i = 0; i < word.length; i++) {
    post = post + "-";
  }
  headerTarget.innerHTML = post;
  buttonSendWord.disabled = true;
  wordInput.disabled = true;
  letterInput.disabled = false;
  buttonSend.disabled = false;
  letterInput.select();
}

// Reset everything back to default
// Show warning on first click, reset on second
function restart() {
  restartClicked++;
  if (restartClicked >= 2) {
    imageTarget.setAttribute("src", "/images/loop.gif");
    letter = undefined;
    word = undefined;
    post = "";
    posted = false;
    attempts = 0;
    correct = false;
    restartClicked = 0;
    headerTarget.innerHTML = "Hangman";

    restartWarning.hidden = true;

    wordInput.disabled = false;
    buttonSendWord.disabled = false;
    buttonSend.disabled = true;
    letterInput.disabled = true;
  } else {
    restartWarning.hidden = false;
  }
}
